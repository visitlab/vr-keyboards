# Design and evaluation of alphabetic and numeric input methods for virtual reality

This repository contains source code and other related resources as described in the research paper *Design and evaluation of alphabetic and numeric input methods for virtual reality*.

## Citation
If you happen to use this code or its parts in your resarch, please cite:  
**TBAL**

## Contents of the repository
**Root folder (except '_Handwriting app' directory):** Unity project (version 2019.4.40f1)  
**'_Handwriting app' directory:** Visual Studio solution producing an executable (already included in *Release* folder) used for hand-writing recognition.

The application is developed for HTC Vive (Pro) series of VR HMDs 

### Build notes
Some of the implemented keyboards require cloud services to work properly.
To use these keyboards, you will first have to create a multi-service account for Microsoft Azure AI / Cognitive services. Please keep in mind that creating just STT/Computer vision services will not suffice. You can verify the created service type in the Type column visible in Azure services overview.  
As soon as your service is created, please fill in the Azure Computer Vision cloud-based service login information into the `Assets/StreamingAssets/CLOUD_SERVICES.txt` and `_Handwriting app\MyhandWritingRecognition\MyhandWritingRecognition\bin\CLOUD_SERVICES.txt` files.

## Licensing
The source code is based on the following bachelor's thesis:

_ROZIČ, Roman. Methods for Alphanumeric Input in Virtual Reality [online]. Brno, 2022 [cit. 2024-03-25]. Available from: https://is.muni.cz/th/l0045/. Bachelor's thesis. Masaryk University, Faculty of Informatics. Supervisor David KUŤÁK._

Additionaly, the code uses following open-source components:
- Source Code Pro font
- Newtonsoft JSON
- Microsoft Cognitive Services SDKs
- Microsoft Computer Vision SDK
- Skybox textures (CC0-licensed) from https://hdrihaven.com/
