﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using System.Collections;

public class SwipeKeyboard : MonoBehaviour
{
    // FreqDictionary = { word : frequency of the use of this word in corpus }
    private Dictionary<string, int> freqDictionary = new Dictionary<string, int>();

    // Indexed dictionary for a tuple of chars (a, b) returns a list of words, where the first letter of a word is 'a' and last letter is 'b'
    private Dictionary<char, Dictionary<char, List<string>>> indexDictionary = new Dictionary<char, Dictionary<char, List<string>>>();

    // Simulated paths dictionary for any string of length 2 containing only [a-z] characters returns simulated path for the given string
    private Dictionary<string, List<Vector3>> pathDictionary = new Dictionary<string, List<Vector3>>();

    // Dictionary data file paths
    private string freqDictionaryData;
    private string simulatedPathDictData;
    private string indexDictData;

    private LineController  lineController;     // line handler
    public GameObject keyboard;                 // keyboard gameObject containing all the keys
    public GameObject[] drawCanvas = new GameObject[3];               // draw canvas where the line is drawn
    
    private TextField       textField;
    private VRControllers   controllers;
    public XRRayInteractor  rayInteractorLeft, rayInteractorRight;  // ray interactors of both controllers

    private const string    alphabet    = "abcdefghijklmnopqrstuvwxyz";
    private bool            isRight     = false;    // checks whether the line is made by a right or left hand

    private void Start()
    {
        freqDictionaryData = Application.dataPath + "/StreamingAssets/Corpus/freq.dict";
        simulatedPathDictData = Application.dataPath + "/StreamingAssets/Corpus/simulatedPath.dict";
        indexDictData = Application.dataPath + "/StreamingAssets/Corpus/indexed.dict";

        lineController = FindObjectOfType<LineController>();
        textField      = FindObjectOfType<TextField>();
        controllers    = FindObjectOfType<VRControllers>();

        LoadDictionaries();
    }

    /// <summary>
    /// Makes a single keystroke on the virtual keyboard. First, takes raycast hit object from the ray interactor and tries
    /// to invoke the onSelectEntered functionality of a single button of the keyboard.
    /// </summary>
    /// <param name="isRight"> Decides whether to call raycast hit from right or left ray interactor. </param>
    private void SingleLetterDrawn(bool isRight)
    {
        RaycastHit hit;
        if (isRight)
        {
            rayInteractorRight.GetCurrentRaycastHit(out hit);
        } else
        {
            rayInteractorLeft.GetCurrentRaycastHit(out hit);
        }
        
        if (hit.collider != null)   // call keyboard event from interactable component
        {
            XRSimpleInteractable interactable = hit.collider.gameObject.GetComponent<XRSimpleInteractable>();
            interactable.onSelectEntered.Invoke((isRight) ? rayInteractorRight : rayInteractorLeft);
        } 
    }

    /// <summary>
    /// Fills empty strings to the candidates list, if its size is lower than 'min' candidates.
    /// </summary>
    /// <param name="candidates"> The list to be filled. </param>
    /// <param name="min"> The minimum size of the list. </param>
    private List<string> FillVoid(List<string> candidates, int min)
    {
        while (candidates.Count < min)
        {
            candidates.Add("");
        }
        return candidates;
    }

    /// <summary>
    /// Enters a word to the text field, corresponding to the drawn line on the canvas. In addition, fills alternative words
    /// as suggestions to the suggestion boxes. The word as well as the suggestions are capitalized, if 'Shift' button is 
    /// active.
    /// </summary>
    private void SingleWordDrawn()
    {
        // retrieve points from the drawn line
        Vector3[] path = new Vector3[lineController.handle.positionCount];
        lineController.handle.GetPositions(path);
        
        // Get exactly 6 candidate words and capitalize candidates, depending on the 'shift' button
        List<string> candidates = NGramSuggester.Capitalize( FillVoid( GetCandidates( path.ToList<Vector3>() ), 6 ), 
                                                             keyboard.transform.Find("a").transform.Find("a").gameObject.GetComponent<Text>().text);
        
        // In the case that nothing is recognized, do not add anything
        if (string.IsNullOrEmpty(candidates[0]) || string.IsNullOrWhiteSpace(candidates[0]))
        {
            return;
        }

        // Place the candidates into the text field
        textField.AddString((textField.GetCurrentWord(true) == "") ? candidates[0] : " " + candidates[0]);
        Suggestions suggestions = FindObjectOfType<Suggestions>();
        for (int i = 1; i < 6; ++i)
        {
            suggestions.PlaceSuggestion(i - 1, candidates[i]);
        }

        if (keyboard.transform.Find("a").transform.Find("a").GetComponent<SingleAlphaButtonScript>().IsShiftActive())   
        {
            FindObjectOfType<ShiftScript>().ShiftTriggered();   // Return shift to default state
        }
    }

    /// <summary>
    /// Tries drawing a line on the canvas from the currently used controller.
    /// </summary>
    /// <param name="_isRight"> Sets the right or left controller as currently used. </param>
    private void TryDraw(bool _isRight)
    {
        RaycastHit hit;
        if (_isRight)
        {
            rayInteractorRight.GetCurrentRaycastHit(out hit);
        } else
        {
            rayInteractorLeft.GetCurrentRaycastHit(out hit);
        }

        if (hit.collider == null || hit.collider.gameObject.name != "DrawCanvas")
        {
            return;
        }
        lineController.Draw(hit.point);
        isRight = _isRight;
    }

    private void Update()
    {
        if (controllers.IsRightTrigger() && drawCanvas[0].activeSelf)    // start drawing on a canvas
        {
            TryDraw(true);
        } else if (controllers.IsLeftTrigger() && drawCanvas[0].activeSelf)
        {
            TryDraw(false);
        } else if (lineController.handle != null && drawCanvas[0].activeSelf) // trigger is released and something is drawn -> lock canvas
        {
            foreach (GameObject o in drawCanvas)
            {
                o.SetActive(false);
            }
        } else if (lineController.handle != null && !drawCanvas[0].activeSelf)
        {
            lineController.handle.Simplify(0.003f);  // simplify path by removing redundant points
            
            if (lineController.GetLineLength() < 0.37f)  
            {
                SingleLetterDrawn(isRight);    // if a line is too short, enter a character
            } else
            {
                SingleWordDrawn();
            }
            Destroy(lineController.handle.gameObject);  // clean the line afterwards
            foreach (GameObject o in drawCanvas)
            {
                o.SetActive(true);
            }

            if (isRight)
            {
                controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
            } else
            {
                controllers.SendLeftHapticImpulse(0, 1.0f, 0.0f);
            }
        }
    }

    /// <summary>
    /// Creates indexed dictionary by traversing through all known words from frequency dictionary and assigning them
    /// to their correct index. The value of indexed dictionary is defined as: IndexedDict[a][b] = Set of words that
    /// starts with a letter 'a' and ends with a letter 'b'. Afterwards, the dictionary is serialized so that it does
    /// not need to be created everytime somebody opens the application.
    /// </summary>
    private void CreateIndexedDict()
    {
        foreach (string s in freqDictionary.Keys)
        {
            char first = s[0], last = s[s.Length - 1];

            if (!indexDictionary.ContainsKey(first))
            {
                indexDictionary.Add(first, new Dictionary<char, List<string>>());
            }
            if (!indexDictionary[first].ContainsKey(last))
            {
                indexDictionary[first].Add(last, new List<string>());
            }
            indexDictionary[first][last].Add(s);
        }
        CollectionSerializer.Serialize(indexDictionary, File.Open(indexDictData, FileMode.Create));
    }

    /// <summary>
    /// Loads all dictionaries so that they can be put into use. If a dictionary file is not found on the disc, then the
    /// dictionary has to be created from scratch.
    /// </summary>
    void LoadDictionaries()
    {
        if (!File.Exists(freqDictionaryData))
        {
            NGramSuggester.LoadCorpusData();
        } 
        freqDictionary = CollectionSerializer
            .Deserialize<Dictionary<string, int>>(File.Open(freqDictionaryData, FileMode.Open));

        if (!File.Exists(indexDictData))
        {
            CreateIndexedDict();
        } else
        {
            indexDictionary = CollectionSerializer
                .Deserialize<Dictionary<char, Dictionary<char, List<string>>>>(File.Open(indexDictData, FileMode.Open));
        }

        if (!File.Exists(simulatedPathDictData))
        {
            IndexAllPaths();
            StoreSimulatedPathDictData();
        } else
        {
            Dictionary<string, List<float>> floats = CollectionSerializer
                .Deserialize<Dictionary<string, List<float>>>(File.Open(simulatedPathDictData, FileMode.Open));
            ReadSimulatedPathDictData(floats);
        }
    }

    /// <summary>
    /// Makes all possible paths from one letter to another letter of the alphabet and stores these simulated paths to the
    /// dictionary.
    /// </summary>
    private void IndexAllPaths()
    {
        foreach (char a in alphabet)
        {
            foreach (char b in alphabet)
            {
                string s = a.ToString() + b.ToString();
                if (!pathDictionary.ContainsKey(s))
                {
                    pathDictionary.Add(s, MyLineUtility.SimulateWordLine(s, keyboard));
                }
            }
        }
    }

    /// <summary>
    /// A 3D point represented as Vector3 cannot be serialized, thus it is converted into 3 float values and in this 
    /// representation the dictionary data are being serialized.
    /// </summary>
    private void StoreSimulatedPathDictData()
    {
        Dictionary<string, List<float>> floats = new Dictionary<string, List<float>>();

        foreach (string s in pathDictionary.Keys)
        {
            floats.Add(s, new List<float>());

            foreach (Vector3 vec in pathDictionary[s])
            {
                floats[s].Add(vec.x);
                floats[s].Add(vec.y);
                floats[s].Add(vec.z);
            }
        }
        CollectionSerializer.Serialize(floats, File.Open(simulatedPathDictData, FileMode.Create));
    }

    /// <summary>
    /// Reads serialized simulated paths data (where a point is represented as 3 float values) and fills 
    /// the simulated paths dictionary.
    /// </summary>
    /// <param name="floats"> Deserialized float values. </param>
    private void ReadSimulatedPathDictData(Dictionary<string, List<float>> floats)
    {
        foreach (string s in floats.Keys)
        {
            pathDictionary.Add(s, new List<Vector3>());

            for (int i = 0; i < floats[s].Count; i += 3)
            {
                pathDictionary[s].Add(new Vector3(floats[s][i], floats[s][i + 1], floats[s][i + 2]));
            }
        }
    }

    /// <summary>
    /// Returns a list of nearby keys from a selected position on the virtual keyboard.
    /// </summary>
    /// <param name="position"> The chosen selected position. </param>
    /// <param name="distance"> Defines the radius for nearby keys. </param>
    private List<char> KeysNearby(Vector3 position, float distance)
    {
        List<char> keys = new List<char>();
        foreach (char a in alphabet)
        {
            if (Vector3.Distance(keyboard.transform.Find(a.ToString()).transform.position, position) < distance)
            {
                keys.Add(a);
            }
        }
        return keys;
    }

    /// <summary>
    /// Returns a dictionary that for each candidate word returns its overall distance between the path drawn by user and
    /// the simulated path of the candidate word. The shorter distance, the better candidate.
    /// </summary>
    /// <param name="path"> User's drawn path. </param>
    /// <param name="candidates"> Candidate words that are being simulated and compared with the user's drawn path. </param>
    private Dictionary<string, float> FillDistancesOfAllCandidates(List<Vector3> path, List<string> candidates)
    {
        Dictionary<string, float> distances = new Dictionary<string, float>();

        foreach (string s in candidates)
        {
            List<Vector3> simulatedPath = new List<Vector3>();
            if (s.Length == 1)
            {
                simulatedPath.AddRange(pathDictionary[s + s]);
            }

            for (int i = 0; i < s.Length - 1; ++i)
            {
                if (i == 0)
                {
                    simulatedPath.AddRange( pathDictionary[s[i].ToString() + s[i + 1].ToString()] );
                } else
                {
                    simulatedPath.AddRange( pathDictionary[s[i].ToString() + s[i + 1].ToString()].Skip(1) );
                }
            }
            distances.Add(s, MyLineUtility.GadDistance(path, simulatedPath) + freqDictionary[s] * 0.001f);
        }
        return distances;
    }
    
    /// <summary>
    /// Firstly, retrieves all nearby keys at the start and the end of the empirical path. Secondly, retrieves all 
    /// possible candidate words, depending on the nearby keys. Thirdly, fills the distances between simulated path
    /// of each candidate word with the empirical path. Finally, the candidate words are ordered according to their 
    /// distances (shorter is better) and returns the 6 (or less) best candidates for the empirical path.
    /// </summary>
    /// <param name="empiricalPath"> The user's drawn path. </param>
    private List<string> GetCandidates(List<Vector3> empiricalPath)
    {
        List<char>      starts = KeysNearby(empiricalPath[0], 0.3f),
                        ends   = KeysNearby(empiricalPath[empiricalPath.Count - 1], 0.3f);

        List<string>    candidates = new List<string>();

        foreach (char a in starts)
        {
            foreach(char b in ends)
            {
                try
                {
                    candidates.AddRange(indexDictionary[a][b]);
                } catch (KeyNotFoundException)
                {
                    continue;
                }
            }
        }
        Dictionary<string, float> distances = FillDistancesOfAllCandidates(empiricalPath, candidates);
        return candidates.OrderBy(a => distances[a]).ToList<string>().Take(6).ToList();
    }
}
