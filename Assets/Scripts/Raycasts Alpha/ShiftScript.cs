﻿using UnityEngine;

public class ShiftScript : MonoBehaviour
{
    private GameObject keyboard;    // keyboard gameObject

    void Start()
    {
        keyboard = GameObject.Find("Keyboard canvas");
    }

    /// <summary>
    /// Triggers "shift" and updates state of all keys in a keyboard.
    /// </summary>
    public void ShiftTriggered()
    {
        foreach (Transform child in keyboard.transform)
        {
            GameObject o = child.gameObject;
            if (o.name.Substring(0, (o.name.Length < 10) ? o.name.Length : 10) == "Background")
            {
                continue;
            }

            SingleAlphaButtonScript script = child.transform.GetChild(0).gameObject.GetComponent<SingleAlphaButtonScript>();
            if (script != null && script.enabled)
            {
                script.Shift();
            }
        }
    }
}
