﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

/// <summary>
/// Utility class that serializes and deserializes an object, or an entire graph of connected objects, in binary format.
/// </summary>
static class CollectionSerializer
{
    public static void Serialize<Object>(Object dictionary, Stream stream)
    {
        try
        {
            using (stream)
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream, dictionary);
            }
        }
        catch (IOException) 
        { 
            Debug.LogError("Data serialization error"); 
        }
    }

    public static Object Deserialize<Object>(Stream stream) where Object : new()
    {
        Object ret = CreateInstance<Object>();
        try
        {
            using (stream)
            {
                BinaryFormatter bin = new BinaryFormatter();
                ret = (Object)bin.Deserialize(stream);
            }
        }
        catch (IOException)
        {
            Debug.LogError("Data deserialization error");
        }
        return ret;
    }

    public static Object CreateInstance<Object>() where Object : new()
    {
        return (Object)Activator.CreateInstance(typeof(Object));
    }
}
