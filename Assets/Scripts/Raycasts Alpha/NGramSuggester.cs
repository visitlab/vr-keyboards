﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEngine;

/*
 * The N-Gram Suggestor class is based on algorithms described in following article: 
 * https://medium.com/related-works-inc/autosuggest-retrieval-data-structures-algorithms-3a902c74ffc8
 * 
 * The original article of Levenshtein distance algorithm:
 * https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#C#
 */

static class NGramSuggester
{
    // Paths to all data files
    private static readonly string corpusTextFilePath = Application.dataPath + "/StreamingAssets/Corpus/unigram_freq.txt";
    private static readonly string gramDictFilePath   = Application.dataPath + "/StreamingAssets/Corpus/gram.dict";
    private static readonly string freqDictFilePath   = Application.dataPath + "/StreamingAssets/Corpus/freq.dict";

    // gramDictionary = { n-gram : {Set of words containing this n-gram} }
    private static Dictionary<string, HashSet<string>> gramDictionary = new Dictionary<string, HashSet<string>>();

    // freqDictionary = { word : frequency of the use of this word in corpus }
    private static Dictionary<string, int> freqDictionary = new Dictionary<string, int>();

    /// <summary>
    /// Returns all n-grams of a word, e.g. ngrams(“food”, 2) == [“_f”, “fo”, “oo”, “od”].
    /// </summary>
    /// <param name="word"> The word being processed. </param>
    /// <param name="n"> The length of a single gram. </param>
    /// <param name="sentinel"> The character being placed at the start of a gram, if its length is shorter than n. </param>
    private static List<string> Ngrams(string word, int n = 2, string sentinel = "_")
    {
        string padding = "";
        
        for (int i = 0; i < n - 1; ++i)
        {
            padding += sentinel;
        }
        word = padding + word;

        int end = word.Length - n + 1;
        List<string> ngrams = new List<string>();

        for (int i = 0; i < end; ++i)
        {
            ngrams.Add(word.Substring(i, n));
        }
        return ngrams;
    }

    /// <summary>
    /// Adds a word to our gram dictionary.
    /// </summary>
    /// <param name="word"> Word being indexed in the gram dictionary. </param>
    private static void Index(string word)
    {
        List<string> grams = Ngrams(word);

        foreach (string ngram in grams)
        {
            if (gramDictionary.ContainsKey(ngram))
            {
                gramDictionary[ngram].Add(word);
                continue;
            }
            
            gramDictionary[ngram] = new HashSet<string>() { word };
        }
    }

    /// <summary>
    /// Insures that the suggestions have a good match percentage with the prefix word and that the number of 
    /// suggestions is exactly of size 'len'.
    /// </summary>
    /// <param name="suggestions"> Provided suggestions. </param>
    /// <param name="match"> The required match percentage with the prefix. </param>
    /// <param name="gramCount"> The total number of grams of the prefix. </param>
    /// <param name="len"> Required number of returned suggestions. </param>
    private static List<string> GetMatchedSuggestions(Dictionary<string, int> suggestions, float match, int gramCount, int len)
    {
        List<string> result = new List<string>();
        foreach (KeyValuePair<string, int> item in suggestions)
        {
            float percentage = item.Value * 1.0f / gramCount;
            if (percentage > match)
            {
                result.Add(item.Key);
            }
        }

        if (result.Count < len && match < 0.05f)
        {
            while (result.Count != len)    // fill empty strings
            {
                result.Add("");
            }
            return result;
        } else if (result.Count < len && match >= 0.05f) 
        { 
            return GetMatchedSuggestions(suggestions, match - 0.2f, gramCount, len);
        }
        return result.OrderByDescending(s => suggestions[s]).ToList();
    }

    /// <summary>
    /// Suggest is the core function of the NGramSuggester and returns the best suggestions for the "prefix" word, 
    /// according to the data corpus.
    /// </summary>
    /// <param name="prefix"> Prefix word being processed. </param>
    /// <param name="size"> (Optional) The number of suggestions being returned. </param>
    /// <param name="match_percentage"> (Optional) Required match percentage of the prefix and a suggested word. </param>
    /// <returns> Returns 'size' suggestions, which have at least 'match_percentage' match percentage with the 'prefix' 
    /// word. </returns>
    public static List<string> Suggest(string prefix, int size = 5, float match_percentage = 0.6f)
    {
        List<string> grams = Ngrams(prefix.ToLower());
        Dictionary<string, int> suggestions = new Dictionary<string, int>();

        foreach (string ngram in grams)
        {
            if (gramDictionary.ContainsKey(ngram))
            {
                foreach (string s in gramDictionary[ngram])
                {
                    if (suggestions.ContainsKey(s))
                    {
                        ++suggestions[s];
                        continue;
                    }
                    suggestions.Add(s, 1);
                }
            }
        }

        List<string> result = GetMatchedSuggestions(suggestions, match_percentage, grams.Count, size)
                                .OrderBy(s => freqDictionary[s] + LevenshteinDistance(s, prefix.ToLower()) * 1000)
                                .Take(size)
                                .ToList();
        return Capitalize(result, prefix);
    }

    /// <summary>
    /// Capitalizes strings, depending on the prefix capitalization. If the prefix is capitalized, then strings from 
    /// 'result' are capitalized.
    /// </summary>
    /// <param name="result"> The list of strings being capitalized. </param>
    /// <param name="prefix"> The prefix string. </param>
    public static List<string> Capitalize(List<string> result, string prefix)
    {
        if (prefix.Length == 0 || prefix[0] != char.ToUpper(prefix[0]))
        {
            return result;
        }

        for (int i = 0; i < result.Count; ++i)
        {
            if (result[i].Length == 1)
            {
                result[i] = result[i].ToUpper();
            }
            else if (result[i].Length > 1)
            {
                result[i] = char.ToUpper(result[i][0]) + result[i].Substring(1);
            }
        }
        return result;
    }

    /// <summary>
    /// Loads corpus data from a text file, process them to n-grams and index them to our ngram dictionary.In addition, freq
    /// dictionary is created as well.
    /// </summary>
    public static void ReadCorpusFromTextFile()
    {
        if ( !File.Exists(corpusTextFilePath) )
        {
            Debug.LogError("Cannot find data TXT file.");
            return;
        }

        int i = 1;
        foreach (Match m in Regex.Matches(File.ReadAllText(corpusTextFilePath).ToLower(), "[a-z]+"))
        {
            if (freqDictionary.ContainsKey(m.Value))
            {
                continue;
            }

            if (i > 150000)
            {
                break;
            }

            Index(m.Value);
            freqDictionary.Add(m.Value, i++);
        }
    }

    /// <summary>
    /// Computes the edit distance of 2 given words.
    /// </summary>
    /// <param name="source"> The source string. </param>
    /// <param name="target"> The target string </param>
    public static int LevenshteinDistance(string source, string target)
    {
        if (string.IsNullOrEmpty(source))
        {
            if (string.IsNullOrEmpty(target)) return 0;
            return target.Length;
        }
        if (string.IsNullOrEmpty(target)) return source.Length;

        if (source.Length > target.Length)
        {
            var temp = target;
            target = source;
            source = temp;
        }

        var m = target.Length;
        var n = source.Length;
        var distance = new int[2, m + 1];
        // Initialize the distance matrix
        for (var j = 1; j <= m; j++) distance[0, j] = j;

        var currentRow = 0;
        for (var i = 1; i <= n; ++i)
        {
            currentRow = i & 1;
            distance[currentRow, 0] = i;
            var previousRow = currentRow ^ 1;
            for (var j = 1; j <= m; j++)
            {
                var cost = (target[j - 1] == source[i - 1] ? 0 : 1);
                distance[currentRow, j] = Mathf.Min(Mathf.Min(
                            distance[previousRow, j] + 1,
                            distance[currentRow, j - 1] + 1),
                            distance[previousRow, j - 1] + cost);
            }
        }
        return distance[currentRow, m];
    }

    /// <summary>
    /// Tries to load corpus data from already processed dictionaries, which are saved on disc. If this is not possible, then
    /// the dictionaries are being created from the original corpus text file, which is way more slower and computationally 
    /// expensive.
    /// </summary>
    public static void LoadCorpusData()
    {
        if ( !File.Exists(gramDictFilePath) || !File.Exists(freqDictFilePath) )
        {
            Debug.Log("Cannot find data files, training suggester from TXT corpus has started!");
            ReadCorpusFromTextFile();
            
            CollectionSerializer.Serialize(gramDictionary, File.Open(gramDictFilePath, FileMode.Create));
            CollectionSerializer.Serialize(freqDictionary, File.Open(freqDictFilePath, FileMode.Create));
            return;
        }
        gramDictionary = CollectionSerializer.Deserialize<Dictionary<string, HashSet<string>>>(File.Open(gramDictFilePath, FileMode.Open));
        freqDictionary = CollectionSerializer.Deserialize<Dictionary<string, int>>(File.Open(freqDictFilePath, FileMode.Open));
    }
}
