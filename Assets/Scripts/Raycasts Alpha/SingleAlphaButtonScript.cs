﻿using UnityEngine;
using UnityEngine.UI;

public class SingleAlphaButtonScript : MonoBehaviour
{
    private TextField textField;
    private Text textComponent;     // the letter or the suggestion of this button
    private bool activeShift;       // is shift applied?

    void Start()
    {
        textField = FindObjectOfType<TextField>();
        textComponent = GetComponent<Text>();
        activeShift = false;
    }

    /// <summary>
    /// Adds the string of this button into the text field.
    /// </summary>
    public void WriteThisAlphaChar()
    {
        textField.AddCharacter(textComponent.text);
        if (activeShift)
        {
            FindObjectOfType<ShiftScript>().ShiftTriggered();
        }
    }

    /// <summary>
    /// Replaces current word (from text field) by suggested word of this game object.
    /// </summary>
    public void ReplaceCurrentWord()
    {
        textField.ReplaceCurrentWord(textComponent.text + " ");
    }

    /// <summary>
    /// Applies "shift" key to the character/suggestion of this game object.
    /// </summary>
    public void Shift()
    {
        if (!this.gameObject.activeSelf || textComponent == null || textComponent.text.Equals(""))
        {
            return;
        }

        if (activeShift)
        {
            textComponent.text = textComponent.text.ToLower();
        } else
        {
            textComponent.text = textComponent.text.Substring(0, 1).ToUpper() + textComponent.text.Substring(1);
        }
        activeShift = !activeShift;     // update shift value
    }

    /// <summary>
    /// Returns whether the letter of this button is currently capitalized.
    /// </summary>
    public bool IsShiftActive()
    {
        return activeShift;
    }
}
