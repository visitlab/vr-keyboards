﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Suggestions : MonoBehaviour
{
    private TextField textField;
    public Text[] textComponents = new Text[5];     // 5 suggestion boxes shown to the user

    private const int stringThresholdLength = 12, 
                      normalSize = 40,              // font size if the length of text is below or equal to 12 characters
                      smallSize = 25;               // font size if the length of text is above 12 characters

    void Start()
    {
        textField = FindObjectOfType<TextField>();
        NGramSuggester.LoadCorpusData();    // prepares the suggester
    }

    /// <summary>
    /// Invokes suggest capability of NGramSuggester for current word from text field.
    /// </summary>
    public void Suggest()
    {
        string current = textField.GetCurrentWord(false);
        if (string.IsNullOrEmpty(current) || current.Length > 25)
        {
            return;
        }
        List<string> suggestions = NGramSuggester.Suggest(current);
        for (int i = 0; i < 5; ++i)
        {
            PlaceSuggestion(i, suggestions[i]);
        }
    }

    /// <summary>
    /// Places string into the suggestion box shown to the user.
    /// </summary>
    /// <param name="i"> The index of the suggestion box, where the word is put into. </param>
    /// <param name="word"> The word being placed into the suggestion box. </param>
    public void PlaceSuggestion(int i, string word)
    {
        textComponents[i].text = word;
        textComponents[i].fontSize = (textComponents[i].text.Length <= stringThresholdLength) ? normalSize : smallSize;
    }
}
