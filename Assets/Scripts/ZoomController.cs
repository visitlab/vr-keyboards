﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ZoomController : MonoBehaviour
{
    private VRControllers controllers;

    public GameObject rightHand, leftHandParent, rightHandParent, sphere, keyboard;
    public XRRayInteractor rayInteractor;

    void Start()
    {
        controllers = FindObjectOfType<VRControllers>();
    }

    void Update()
    {
        // freeze rotation of right controller
        Vector3 vec = controllers.GetRightPosition();
        rightHandParent.transform.rotation = Quaternion.Inverse(controllers.GetRightRotation());    
        rightHand.transform.position = vec;

        // Mirror position (X and Y axis) of right controller with these game objects
        transform.position = new Vector3(rightHand.transform.position.x, rightHand.transform.position.y, transform.position.z);
        leftHandParent.transform.position = transform.position + new Vector3(0.0f, 0.0f, 35f);
        sphere.transform.position = transform.position;

        if (controllers.IsRightTrigger())   // a keystroke 
        {
            rayInteractor.GetCurrentRaycastHit(out RaycastHit hit);
            if (hit.collider != null)
            {
                XRSimpleInteractable interactable = hit.collider.gameObject.GetComponent<XRSimpleInteractable>();
                interactable.onSelectEntered.Invoke(rayInteractor);     // call keyboard event from interactable component
                controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
            }
        }
        if (controllers.IsRightPrimary())    // move keyboard position
        {
            keyboard.transform.position = new Vector3(transform.position.x, transform.position.y, keyboard.transform.position.z);
            controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
        }
    }
}
