﻿using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// Provides text editing functionality for the text field game object.
/// </summary>
public class TextField : MonoBehaviour
{ 
    private const int fieldMinBound = 0;    // min length of a text, visible by user (empty text field)
    public int fieldMaxBound = 31;          // max -//-                              (full text field)

    private string text;        // whole input text
    private Text shownText;     // only a portion of the text that is visible by user
    private int textOffset;     // number of hidden characters from the left (text is longer than the length of text field)
    
    public Transform background;
    public Transform cursorTransform;   // cursor transform required for updating position of the cursor
    public GameObject arrowLeft;
    public GameObject arrowRight;
    private int cursorPos;              // actual cursor position in the text
    private Vector3 cursorXOffset;      // cursor is moved, left or right, by this vector

    private float cursorStartX;
    private TestingMode testingMode;

    void Start()
    {
        textOffset = 0;

        shownText = this.gameObject.GetComponent<Text>();
        cursorPos = 0;
        text = "";

        // Set the desired length of the text field depending on the field max bound
        cursorStartX = cursorTransform.localPosition.x;
        background.localScale = new Vector3(background.localScale.x / 31 * fieldMaxBound, background.localScale.y, background.localScale.z);
        RectTransform rect = GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x / 31 * fieldMaxBound, rect.sizeDelta.y);
        cursorTransform.localPosition = new Vector3(cursorStartX / 31 * fieldMaxBound, cursorTransform.localPosition.y, cursorTransform.localPosition.z);
        cursorXOffset = new Vector3(Mathf.Abs(cursorTransform.position.x * 2) / fieldMaxBound, 0.0f, 0.0f);

        FindObjectOfType<ChangeSkybox>().Edit(FindObjectOfType<ChangeSkybox>().AppliedSkybox());
    }

    /// <summary>
    /// Updates currently shown text to the user that depends on its length and the textOffset. 
    /// </summary>
    private void UpdateShownText()
    {
        int lenOfShownText = text.Length - textOffset;
        shownText.text = text.Substring(textOffset, (lenOfShownText > fieldMaxBound) ? fieldMaxBound : lenOfShownText);

        arrowLeft.SetActive(textOffset > fieldMinBound);
        arrowRight.SetActive(textOffset + fieldMaxBound < text.Length);
    }

    /// <summary>
    /// Moves cursor to the left by one character. There are 3 main cases that could happen:
    /// 1. Cursor is at the beginning of the string -> no action happens.
    /// 2. Cursor is at the beinning of the text field + 1 char and textOffset > 0 -> decrement the textOffset and move the 
    ///    text to the left by one character.
    /// 3. Cursor is somewhere in the middle of text field -> move the cursor to the left by one character.
    /// </summary>
    public void MoveCursorLeft()
    {
        if (cursorPos == fieldMinBound)
        {
            return;
        } else if (cursorPos == textOffset + 1 && textOffset != fieldMinBound)  // at the beginning of text field
        {
            --textOffset;
        } else if(cursorPos - textOffset <= fieldMaxBound)  // at the middle of text field
        {
            cursorTransform.Translate(- cursorXOffset );
        }

        --cursorPos;
        UpdateShownText();
    }

    /// <summary>
    /// Moves cursor to the right by one character. There are 3 main cases that could happen:
    /// 1. Cursor is at the end of the string -> no action happens.
    /// 2. Cursor is somewhere in the middle of text field -> move the cursor to the right by one character.
    /// 3. Cursor is at the end of the text field - 1 char and the string is longer than the text that is currently shown -> 
    ///    increment the textOffset and move the text to the right by one character.
    /// </summary>
    public void MoveCursorRight()
    {
        if (cursorPos == text.Length)
        {
            return;
        } else if (cursorPos - textOffset < fieldMaxBound - 1)  // at the middle of text field
        {
            cursorTransform.Translate( cursorXOffset );
        } else   // at the end of text field
        {
            ++textOffset;
        }

        ++cursorPos;
        UpdateShownText();
    }

    /// <summary>
    /// Adds one character to the current position of the cursor.
    /// </summary>
    /// <param name="c"> The character being added to the text field. </param>
    public void AddCharacter(string c)
    {
        if (c.Length != 1)  // c is of type string because of the unity editor - it is more convenient than char
        {
            Debug.LogError("Trying to add more than one character or an empty character");
            return;
        }

        if (cursorPos == text.Length)   // add to the end
        {
            text = text.Substring(0, cursorPos) + c;
        } else   // add to the middle
        {
            text = text.Substring(0, cursorPos) + c + text.Substring(cursorPos);
        }
        MoveCursorRight();

        testingMode = FindObjectOfType<TestingMode>();
        if (testingMode != null)
        {
            testingMode.CheckMistakes(false);
        }
    }

    /// <summary>
    /// Backspace functionality from the current position of the cursor.
    /// </summary>
    public void EraseCharacter()
    {
        if (cursorPos == fieldMinBound)
        {
            return;
        }

        if (cursorPos == text.Length)   // erase from the end
        {
            text = text.Substring(0, cursorPos - 1);
        } else   // erase from the middle
        {
            text = text.Substring(0, cursorPos - 1) + text.Substring(cursorPos);
        }
        MoveCursorLeft();
    }

    /// <summary>
    /// Returns {start, end} indices of the word that is currently selected with the cursor.
    /// </summary>
    private Tuple<int, int> GetCurrentWordBounds()
    {
        int begin = cursorPos, 
            end = cursorPos;

        while (begin - 1 >= 0 && text[begin - 1] != ' ')
        {
            --begin;
        }
        while (end < text.Length && text[end] != ' ')
        {
            ++end;
        }
        return new Tuple<int, int>( begin, end );
    }

    /// <summary>
    /// Returns currently selected word by the cursor.
    /// <param name="full"> Determines whether to take the whole current word or just until the cursor position. </param>
    /// </summary>
    public string GetCurrentWord(bool full)
    {
        Tuple<int, int> indices = GetCurrentWordBounds();
        return text.Substring(indices.Item1, ((full) ? indices.Item2 : cursorPos) - indices.Item1);
    }

    /// <summary>
    /// Replaces the current word with another string (can be also used to erase the current word).
    /// </summary>
    /// <param name="_new"> This string will replace the current word. </param>
    public void ReplaceCurrentWord(string _new)
    {
        Tuple<int, int> indices = GetCurrentWordBounds();
        //text = text.Remove(indices.Item1, indices.Item2 - indices.Item1);
        
        while (cursorPos != indices.Item2)
        {
            MoveCursorRight();
        }
        while (cursorPos != indices.Item1)
        {
            EraseCharacter();
        }
        
        UpdateShownText();  // in case of empty _new string
        AddString(_new);
    }

    /// <summary>
    /// Erases currently selected word in the text field and moves the cursor to the previous word.
    /// </summary>
    public void EraseCurrentWord()
    {
        ReplaceCurrentWord("");
        MoveCursorLeftWord();
    }

    /// <summary>
    /// Adds a new word to the current position of the cursor.
    /// </summary>
    /// <param name="w"> The word being added to the text field. </param>
    public void AddString(string w)
    {
        foreach (char c in w)
        {
            AddCharacter(c.ToString());
        }
    }

    /// <summary>
    /// Moves the cursor to a previous word, if it is possible. 
    /// </summary>
    public void MoveCursorLeftWord()
    {
        Tuple<int, int> indices = GetCurrentWordBounds();
        while ( (cursorPos >= indices.Item1 || text[cursorPos] == ' ') && cursorPos > fieldMinBound)
        {
            MoveCursorLeft();
        }
        MoveCursorRight();  // fix position
    }

    /// <summary>
    /// Moves cursor to the next word, if it is possible.
    /// </summary>
    public void MoveCursorRightWord()
    {
        Tuple<int, int> indices = GetCurrentWordBounds();
        while ( (cursorPos <= indices.Item2 || text[cursorPos] == ' ') && cursorPos < text.Length - 1)
        {
            MoveCursorRight();
        }
    }

    /// <summary>
    /// Deletes the textual content of the whole text inputted in the text field.
    /// </summary>
    public void EraseText()
    {
        while (cursorPos <= text.Length - 1)
        {
            MoveCursorRight();
        }
        while (text.Length != 0)
        {
            EraseCharacter();
        }
    }

    /// <summary>
    /// Gets the current textual content of the whole inputted text from the text field.
    /// </summary>
    /// <returns></returns>
    public string GetText() => text;
}
