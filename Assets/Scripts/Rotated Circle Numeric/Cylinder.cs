﻿using UnityEngine;

public class Cylinder : MonoBehaviour
{
    public GameObject leftHand, rightHand;          // controller gameObjects
    public GameObject leftCylinder, rightCylinder;  // cylinder gameObjects

    void Update()
    {
        Vector3 lC = leftCylinder.transform.rotation.eulerAngles;
        Vector3 rC = rightCylinder.transform.rotation.eulerAngles;
        Vector3 lH = leftHand.transform.rotation.eulerAngles;
        Vector3 rH = rightHand.transform.rotation.eulerAngles;

        // Mirror Z-axis rotation of controllers with the cylinders
        leftCylinder.transform.rotation = Quaternion.Euler(lC.x, lC.y, -lH.z);
        rightCylinder.transform.rotation = Quaternion.Euler(rC.x, rC.y, -rH.z);
    }
}
