﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Selector : MonoBehaviour
{
    private VRControllers controllers;
    public bool isLeft;     // is this a selector on a left cylinder?

    private void Start()
    {
        controllers = FindObjectOfType<VRControllers>();
    }

    private void OnTriggerStay(Collider other)
    {
        // if a number collides with this selector and trigger is pressed (for any controllers), make a keystroke
        if ( (isLeft && controllers.IsLeftTrigger()) || (!isLeft && controllers.IsRightTrigger()))
        {
            XRSimpleInteractable interactable = other.gameObject.GetComponent<XRSimpleInteractable>();
            interactable.onSelectEntered.Invoke( FindObjectOfType<XRRayInteractor>() );

            if (!isLeft)
            {
                controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
            }
            else
            {
                controllers.SendLeftHapticImpulse(0, 1.0f, 0.0f);
            }
        }
    }
}
