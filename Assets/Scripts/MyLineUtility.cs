﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Provides numerous utility functions for line handling.
/// </summary>
static class MyLineUtility
{
    /// <summary>
    /// Takes a string, simulates its drawn line, by connecting letters of the word together on the keyboard, and returns
    /// list of points of this line.
    /// </summary>
    /// <param name="s"> The simulating word. </param>
    /// <param name="keyboard"> The keyboard gameObject, where the positions of letters are being found. </param>
    public static List<Vector3> SimulateWordLine(string s, GameObject keyboard)
    {
        List<Vector3> points = new List<Vector3>();

        for (int i = 0; i < s.Length; ++i)
        {
            points.Add(keyboard.transform.Find(s[i].ToString()).transform.position);
        }
        if (s.Length == 1)  // line should have at least 2 endpoints (can be the same)
        {
            points.Add(points[0]);
        }

        return FillPath(points, 0.05f);
    }

    /// <summary>
    /// Returns filled path of points with new interpolated points in order to preserve max distance between points (3D).
    /// </summary>
    /// <param name="source"> The source list of points. </param>
    /// <param name="delta"> max allowed distance between 2 points of a line. </param>
    public static List<Vector3> FillPath(List<Vector3> source, float delta)
    {
        for (int i = 0; i < source.Count() - 1; ++i)
        {
            float d = Vector3.Distance(source[i], source[i + 1]);
            if (d <= delta)
            {
                continue;
            }
            source.InsertRange(i + 1, InterpolateBetweenPoints(source[i], source[i + 1], d, delta));
        }
        return source;
    }

    /// <summary>
    /// Returns filled path of points with new interpolated points in order to preserve max distance between points (2D).
    /// </summary>
    /// <param name="source"> The source list of points. </param>
    /// <param name="delta"> Max allowed distance between 2 points of a line. </param>
    public static List<Vector2> FillPath(List<Vector2> source, float delta)
    {
        for (int i = 0; i < source.Count() - 1; ++i)
        {
            float d = Vector2.Distance(source[i], source[i + 1]);
            if (d <= delta)
            {
                continue;
            }
            source.InsertRange(i + 1, InterpolateBetweenPoints(source[i], source[i + 1], d, delta));
        }
        return source;
    }

    /// <summary>
    /// Linearly interpolates between two points in 3D space.
    /// </summary>
    /// <param name="start"> Starting point. </param>
    /// <param name="end"> Ending point. </param>
    /// <param name="distance"> The distance between the starting and ending point. </param>
    /// <param name="delta"> Max allowed distance between 2 points of a line. </param>
    /// <returns> A list of interpolated points, including the starting and edning point. </returns>
    public static List<Vector3> InterpolateBetweenPoints(Vector3 start, Vector3 end, float distance, float delta)
    {
        // fraction = 1.0f / number of new points
        float fraction = 1.0f / Mathf.Ceil(distance / delta);  
        
        List<Vector3> result = new List<Vector3>();

        for (float t = 0.0f; t <= 1.0f; t += fraction)
        {
            result.Add(Vector3.Lerp(start, end, t));
        }
        return result;
    }

    /// <summary>
    /// Linearly interpolates between two points in 2D space.
    /// </summary>
    /// <param name="start"> Starting point. </param>
    /// <param name="end"> Ending point. </param>
    /// <param name="distance"> The distance between the starting and ending point. </param>
    /// <param name="delta"> Max allowed distance between 2 points of a line. </param>
    /// <returns> A list of interpolated points, including the starting and edning point. </returns>
    public static List<Vector2> InterpolateBetweenPoints(Vector2 start, Vector2 end, float distance, float delta)
    {
        // fraction = 1.0f / number of new points
        float fraction = 1.0f / Mathf.Ceil(distance / delta);  
        
        List<Vector2> result = new List<Vector2>();

        for (float t = 0.0f; t <= 1.0f; t += fraction)
        {
            result.Add(Vector2.Lerp(start, end, t));
        }
        return result;
    }

    /// <summary>
    /// Helper function for the GadDistance fci which finds the best match for a provdided coordinate on a simulated path.
    /// </summary>
    /// <param name="coordinate"> Coordinate from the user's drawn line. </param>
    /// <param name="simulatedPath"> Simulated path. </param>
    /// <param name="index"> Does not consider previous points before "index"... the algorthm is single-pass, does not check
    /// all points everytime it is called. </param>
    /// <returns></returns>
    private static Tuple<float, int> NextBestMatch(Vector3 coordinate, List<Vector3> simulatedPath, int index)
    {
        float bestDistance = Vector3.Distance(simulatedPath[index], coordinate);
        float nextDistance = bestDistance;

        while (nextDistance <= bestDistance && simulatedPath.Count - 1 != index)
        {
            ++index;
            bestDistance = nextDistance;
            nextDistance = Vector3.Distance(simulatedPath[index], coordinate);
        }
        return new Tuple<float, int>(bestDistance, index);
    }

    /// <summary>
    /// Computes distance of 2 paths. The Greedy asymetric dynamic time warping algorithm, as explained by Zoeten. 
    /// Please check the thesis for more details.....
    /// </summary>
    /// <param name="empiricalPath"> The user's drawn path. </param>
    /// <param name="simulatedPath"> The simulated path. </param>
    /// <returns> Returns total distance between 2 lines (lists of points). </returns>
    public static float GadDistance(List<Vector3> empiricalPath, List<Vector3> simulatedPath)
    {
        float sumDistance = Vector3.Distance(empiricalPath[0], simulatedPath[0]);
        int j = 0;
        Tuple<float, int> distance_index;

        for (int i = 1; i < empiricalPath.Count; ++i)
        {
            distance_index = NextBestMatch(empiricalPath[i], simulatedPath, j);
            sumDistance += distance_index.Item1;
            j = distance_index.Item2;
        }
        for (int i = j; i < simulatedPath.Count; ++i)
        {
            sumDistance += Vector3.Distance(empiricalPath[empiricalPath.Count - 1], simulatedPath[i]);
        }

        return sumDistance;
    }
}
