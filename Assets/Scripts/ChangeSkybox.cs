﻿using UnityEngine;

public class ChangeSkybox : MonoBehaviour
{
    public Material[] skyboxes = new Material[6];   // available skyboxes
    private int appliedSkybox;
    public static ChangeSkybox instance;

    /// <summary>
    /// Sets this object as a singleton.
    /// </summary>
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            appliedSkybox = 0;
        } else
        {
            Destroy(this.gameObject);
        }
    }

    /// <summary>
    /// Applies selected material as a new skybox.
    /// </summary>
    /// <param name="number"> Index of a material from skyboxes parameter. </param>
    public void Edit(int number)
    {
        RenderSettings.skybox = skyboxes[number];
        DynamicGI.UpdateEnvironment();
        appliedSkybox = number;
    }

    /// <summary>
    /// Returns index of applied skybox in the scene.
    /// </summary>
    public int AppliedSkybox() => appliedSkybox;
}
