﻿using UnityEngine;

public class TrackpadNumericKeyboard : MonoBehaviour
{
    private VRControllers controllers;
    private TextField textField;

    public GameObject[] highlights = new GameObject[10];    // Highlight gameObjects for all numbers

    private void Start()
    {
        controllers = FindObjectOfType<VRControllers>();
        textField = FindObjectOfType<TextField>();
    }

    void Update()
    {
        // resets all highlights
        foreach (GameObject obj in highlights)  
        {
            obj.SetActive(false);
        }

        // handles number input
        AxisController(controllers.GetLeftTrackpad(), 0, controllers.IsLeftTrackpad());
        AxisController(controllers.GetRightTrackpad(), 5, controllers.IsRightTrackpad());

        // handles other keystrokes
        if (controllers.IsRightPrimary())
        {
            textField.AddCharacter(".");
            controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
        }
        else if (controllers.IsLeftPrimary())
        {
            textField.AddCharacter("-");
            controllers.SendLeftHapticImpulse(0, 1.0f, 0.0f);
        }
        else if (controllers.IsLeftTrigger())
        {
            textField.MoveCursorLeft();
            controllers.SendLeftHapticImpulse(0, 1.0f, 0.0f);
        }
        else if (controllers.IsRightTrigger())
        {
            textField.MoveCursorRight();
            controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
        }
        else if (controllers.IsLeftGrip())
        {
            textField.EraseCharacter();
            controllers.SendLeftHapticImpulse(0, 1.0f, 0.0f);
        }
    }

    /// <summary>
    /// Updates current highlight of a number, which shows which button is currently touched, and handles click 
    /// event for any number from any controller.
    /// </summary>
    /// <param name="axis2D"> Current touch position from the trackpad. </param>
    /// <param name="offset"> 0 for left controller and 5 for right controller. </param>
    /// <param name="click"> Is the trackpad pressed? </param>
    private void AxisController(Vector2 axis2D, int offset, bool click)
    {
        int num = 0;

        if (-0.5f < axis2D[0] && axis2D[0] < 0.5f && -0.5f < axis2D[1] && axis2D[1] < 0.5f)
        {
            num = 2 + offset;
        }
        else if (-0.5f >= axis2D[0] && -0.5f < axis2D[1] && axis2D[1] < 0.5f)
        {
            num = 1 + offset;
        }
        else if (0.5f <= axis2D[0] && -0.5f < axis2D[1] && axis2D[1] < 0.5f)
        {
            num = 3 + offset;
        }
        else if (-0.5f < axis2D[0] && axis2D[0] < 0.5f && -0.5f >= axis2D[1])
        {
            num = 4 + offset;
        }
        else if (-0.5f < axis2D[0] && axis2D[0] < 0.5f && 0.5f <= axis2D[1])
        {
            num = 0 + offset;
        }

        highlights[num].SetActive(true);

        if (!click)
        {
            return;
        }
        
        textField.AddCharacter(num.ToString());
        if (offset == 0)
        {
            controllers.SendLeftHapticImpulse(0, 1.0f, 0.0f);
        } else
        {
            controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
        }
    }
}
