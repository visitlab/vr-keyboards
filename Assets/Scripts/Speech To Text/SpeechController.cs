﻿using UnityEngine;
using SpeechToTextNamespace;
using Microsoft.CognitiveServices.Speech;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;

public class SpeechController : MonoBehaviour
{
    private TextField textField;
    private SpeechRecognizer recognizer;    // Speech recognizer handle
    public GameObject loadingBar;

    void Start()
    {
        textField = FindObjectOfType<TextField>();
        Task t = Task.Run(() =>
        {
            recognizer = SpeechHandle.Authenticate();   // try to login to the recognition service
        });
        if (!t.Wait(System.TimeSpan.FromMilliseconds(5000)))
        {
            Debug.LogError("Failed to authenticate");
            SceneManager.LoadScene("Start");
        }
        Debug.Log("Authenticated");
    }
    
    /// <summary>
    /// Calls the recognizer when the "start" button from the keyboard is pressed, then the recognized input is entered to 
    /// the text field.
    /// </summary>
    public void StartTheSpeech()
    {
        SpeechHandle.RecognizeSpeechAsync(recognizer, textField, loadingBar);
    }
}
