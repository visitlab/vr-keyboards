﻿//
// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE.md file in the project root for full license information.
// https://github.com/Azure-Samples/cognitive-services-speech-sdk
//

using System.Collections;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CognitiveServices.Speech;
using UnityEngine;

namespace SpeechToTextNamespace
{
    class SpeechHandle
    {
        private static string credentialsPath = Application.streamingAssetsPath + "/CLOUD_SERVICES.txt";
        /// <summary>
        /// Creates an instance of a speech config with specified subscription key and service region.
        /// </summary>
        public static SpeechRecognizer Authenticate()
        {
            string subscription = File.ReadAllLines(credentialsPath).Skip(15).Take(1).First();
            string endpoint = File.ReadAllLines(credentialsPath).Skip(16).Take(1).First();
            var speechConfig = SpeechConfig.FromSubscription(subscription, endpoint);
            return new SpeechRecognizer(speechConfig);
        }

        public static async Task RecognizeSpeechAsync(SpeechRecognizer recognizer, TextField textField, GameObject loadBar)
        {
            // Starts speech recognition, and returns after a single utterance is recognized. The end of a
            // single utterance is determined by listening for silence at the end or until a maximum of 15
            // seconds of audio is processed.  The task returns the recognition text as result. 
            // Note: Since RecognizeOnceAsync() returns only a single utterance, it is suitable only for single
            // shot recognition like command or query. 
            // For long-running multi-utterance recognition, use StartContinuousRecognitionAsync() instead.            
            loadBar.SetActive(true);
            var res = await recognizer.RecognizeOnceAsync();

            // Checks result.
            if (res.Reason == ResultReason.RecognizedSpeech)
            {
                string curr = textField.GetCurrentWord(true);
                if (res.Text.Length > 0)
                {
                    textField.AddString(((curr == "") ? "" : " ") + res.Text.ToLower().Substring(0, res.Text.Length - 1));
                }
            }
            loadBar.SetActive(false);
        }
    }
}