﻿using UnityEngine;

public class DiceNumericKeyboard : MonoBehaviour
{
    private TextField textField;
    private VRControllers controllers;

    private bool velocityInUse;
    private const float velocityThreshold = 0.8f;   // determines whether the velocity action should be invoked

    private void Start()
    {
        textField = FindObjectOfType<TextField>();
        controllers = FindObjectOfType<VRControllers>();
        velocityInUse = false;
    }

    private void Update()
    {
        if (controllers.GetRightVelocity().y < -velocityThreshold && !velocityInUse)    // throw right controller down
        {
            velocityInUse = true;
            textField.EraseCharacter();
            controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
        } else if (controllers.GetRightVelocity().x < -velocityThreshold && !velocityInUse)     // throw right controller left
        {
            velocityInUse = true;
            textField.MoveCursorLeft();
            controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
        } else if (controllers.GetRightVelocity().x > velocityThreshold && !velocityInUse)      // throw right controller right
        {
            velocityInUse = true;
            textField.MoveCursorRight();
            controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
        } else if (controllers.GetLeftVelocity().x < -velocityThreshold && !velocityInUse)      // throw left controller left
        {
            velocityInUse = true;
            textField.AddCharacter("-");
            controllers.SendLeftHapticImpulse(0, 1.0f, 0.0f);
        } else if (controllers.GetLeftVelocity().z > velocityThreshold && !velocityInUse)       // throw left controller forward
        {
            velocityInUse = true;
            textField.AddCharacter(".");
            controllers.SendLeftHapticImpulse(0, 1.0f, 0.0f);
        } else if (controllers.GetLeftVelocity().z <= velocityThreshold && controllers.GetLeftVelocity().x >= -velocityThreshold 
            && velocityInUse && controllers.GetRightVelocity().x >= -velocityThreshold && controllers.GetRightVelocity().x <= velocityThreshold && controllers.GetRightVelocity().y >= -velocityThreshold)
        {
            velocityInUse = false;  // velocity not in use
        }
    }
}
