﻿using UnityEngine;

/// <summary>
/// Provides functionality for drawing a line.
/// </summary>
public class LineController : MonoBehaviour
{
    public GameObject linePrefab;   // original prefab of the line
    public LineRenderer handle;     // renderer of the currently drawn line

    /// <summary>
    /// Draws a new point of line to the canvas. If the handle is null, creates a new line with its 
    /// starting point. Otherwise, adds a new point to the existing line.
    /// </summary>
    /// <param name="hit"> A hit point that is added to the line. </param>
    public void Draw(Vector3 hit)
    {
        if (handle != null)
        {
            ++handle.positionCount;
            handle.SetPosition(handle.positionCount - 1, hit);
            return;
        }

        handle = Instantiate(linePrefab, hit, Quaternion.identity).GetComponent<LineRenderer>();
        handle.positionCount = 1;
        handle.SetPosition(0, hit);
    }

    /// <summary>
    /// Returns current line length.
    /// </summary>
    public float GetLineLength()
    {
        float dist = 0.0f;
        for (int i = 0; i < handle.positionCount - 1; ++i)
        {
            dist += Vector3.Distance(handle.GetPosition(i), handle.GetPosition(i + 1));
        }
        return dist;
    }
}
