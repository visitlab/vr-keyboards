﻿using UnityEngine;

/// <summary>
/// Handles the button movement and how it behaves when the button is pushed.
/// </summary>
public class CubeController : MonoBehaviour
{
    private Rigidbody rb;               // rigidbody component of the button
    private Vector3 startPos;           // starting position of the button
    private const float endZ = 3.193f;  // satanic constant representing the max allowed position of a pushed button

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotation;   // freeze rotation of the button gameObject
        startPos = transform.localPosition;
    }

    /// <summary>
    /// Handles the physics and movement of the cube.
    /// </summary>
    void Update()
    {
        // button is pushed and tries to go back
        if (transform.localPosition.z > startPos.z)    
        {
            rb.AddForce(transform.forward * (-2.0f));
        }

        // guard in the case that the button wants to escape forward or backward from his little prison
        if (transform.localPosition.z < startPos.z || transform.localPosition.z >= endZ) 
        {
            transform.localPosition = startPos;
        }

        // freeze translation of X and Y axis in local space, because the button is allowed to move in Z direction only
        transform.localPosition = new Vector3(startPos.x, startPos.y, transform.localPosition.z);   
    }
}
