﻿using UnityEngine;

/// <summary>
/// Handles the current click state of the button.
/// </summary>
public class ButtonController : MonoBehaviour
{
    // the click state
    private bool click;     

    void Start()
    {
        click = false;
    }

    /// <summary>
    /// Changes the click state of the button.
    /// </summary>
    /// <param name="b"> A new state of the click. </param>
    public void UpdateClick(bool b)
    {
        click = b;
    }

    /// <summary>
    /// If a click state is true, then the button is unable to make a keystroke. It needs to return to its default position,
    /// where the click state is updated to false. Only after that, the button is able to make a keystroke.
    /// </summary>
    /// <returns> The current click state of this button. </returns>
    public bool ClickState()
    {
        return click;
    }
}
