﻿using UnityEngine;

public class TriggerPulledController : MonoBehaviour
{
    private ButtonController parent;    // parent object that is responsible for the click state management

    private void Start()
    {
        parent = transform.parent.gameObject.GetComponent<ButtonController>();
    }

    // When the button returns to his default position, the click state resets and a new keystroke can be done.
    private void OnTriggerEnter(Collider other)
    {
        parent.UpdateClick(false);
    }
}
