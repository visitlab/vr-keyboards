﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class TriggerPushedController : MonoBehaviour
{
    private ButtonController parent;            // parent object that is responsible for the click state management
    private XRSimpleInteractable interactable;  // the interactable that holds the keystroke action
    private VRControllers controllers;

    private void Start()
    {
        parent = transform.parent.gameObject.GetComponent<ButtonController>();
        interactable = GetComponent<XRSimpleInteractable>();
        controllers = FindObjectOfType<VRControllers>();
    }

    // when the button is fully pushed, a keystroke is made and also the click state is updated
    private void OnTriggerEnter(Collider other)
    {
        // guard - when a button is not fully released and tries to make a keystroke again
        if (parent.ClickState())    
        {
            return;
        }

        interactable.onSelectEntered.Invoke( FindObjectOfType<XRRayInteractor>() );
        parent.UpdateClick(true);
    }
}
