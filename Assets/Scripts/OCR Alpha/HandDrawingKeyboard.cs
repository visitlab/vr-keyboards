﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using System.IO;
using System;
using System.Diagnostics;
using UnityEngine.UI;
using System.Threading.Tasks;
using System.Threading;

public class HandDrawingKeyboard : MonoBehaviour
{
    private VRControllers con;
    private TextField textField;
    
    public GameObject drawCanvas;               // the canvas, which contains the draw texture
    private Texture2D drawTexture;              // the main draw texture of the canvas
    private LineController lineRenderer;        // used to draw lines on the draw texture
    private bool isDrawn;                       // is a drawing on the texture?
    private List<Vector2> points;               // the drawn points on the canvas
    private Color32[] fillColorArray;           // used to clear the whole texture at once

    public XRRayInteractor rayInteractorLeft, rayInteractorRight;
    public GameObject loadingBar;               // signal that recognition is in the progress

    /// <summary>
    /// Clears the texture by erasing the previous drawing and applying the new changes on the texture. 
    /// </summary>
    public void ClearTexture()
    {
        fillColorArray = drawTexture.GetPixels32();
        for (int i = 0; i < fillColorArray.Length; ++i)
        {
            fillColorArray[i] = Color.white;
        }
        drawTexture.SetPixels32(fillColorArray);
        drawTexture.Apply();
    }

    private void Start()
    {
        con = FindObjectOfType<VRControllers>();
        textField = FindObjectOfType<TextField>();
        lineRenderer = FindObjectOfType<LineController>();

        isDrawn = false;
        drawTexture = drawCanvas.GetComponent<Renderer>().material.mainTexture as Texture2D;
        points = new List<Vector2>();
        ClearTexture();
        RecognizeAsync();
        loadingBar.SetActive(false);
    }

    /// <summary>
    /// Simplifies the set of points in order to make the path more straight. Then it fills the path with interpolated points
    /// to add the missing points in the path. It also draws onto the draw texture with the edited set of points. Moreover, 
    /// then applies the changes on the texture, clears the set of points and destroys the currently drawn line.
    /// </summary>
    private void InterpolateAndSetPixels()
    {
        LineUtility.Simplify(points, 2.5f, points);
        foreach (Vector2 point in MyLineUtility.FillPath(points, 1.0f))
        {
            // fill 4-neighbourhood pixels as well
            drawTexture.SetPixel(Mathf.FloorToInt(point.x), Mathf.FloorToInt(point.y), Color.black);
            drawTexture.SetPixel(Mathf.FloorToInt(point.x - 1), Mathf.FloorToInt(point.y), Color.black);
            drawTexture.SetPixel(Mathf.FloorToInt(point.x + 1), Mathf.FloorToInt(point.y), Color.black);
            drawTexture.SetPixel(Mathf.FloorToInt(point.x), Mathf.FloorToInt(point.y - 1), Color.black);
            drawTexture.SetPixel(Mathf.FloorToInt(point.x), Mathf.FloorToInt(point.y + 1), Color.black);
        }

        drawTexture.Apply();
        points.Clear();
        Destroy(lineRenderer.handle.gameObject);
    }

    /// <summary>
    /// Provides haptic feedback after invoking the recognition.
    /// </summary>
    private void HapticFeedback()
    {
        if (con.IsRightPrimary())
        {
            con.SendRightHapticImpulse(0, 1.0f, 0.0f);
        }
        else
        {
            con.SendLeftHapticImpulse(0, 1.0f, 0.0f);
        }
    }

    private void Update()
    {
        // invokes OCR engine by the grip button and takes the recognized text from the drawing
        if ((con.IsRightPrimary() || con.IsLeftPrimary()) && isDrawn)
        {
            RecognizeAsync();
            HapticFeedback();
            return;
        }

        // applies the drawn line from the canvas as a drawing onto the texture
        if (!con.IsRightTrigger() && !con.IsLeftTrigger() && isDrawn && lineRenderer.handle != null)
        {
            InterpolateAndSetPixels();
            return;
        }
        
        RaycastHit hit;
        if (con.IsRightTrigger())
        {
            rayInteractorRight.GetCurrentRaycastHit(out hit);
        } else if (con.IsLeftTrigger())
        {
            rayInteractorLeft.GetCurrentRaycastHit(out hit);
        } else
        {
            return; // no hit onto the canvas is made, go no further...
        }

        // adds a point to the drawn line on the canvas
        if (hit.collider != null && hit.collider.gameObject.name == "DrawCanvas")
        {
            isDrawn = true;
            lineRenderer.Draw(hit.point + new Vector3(0.0f, 0.0f, -0.005f));
            points.Add(new Vector2(hit.textureCoord.x * drawTexture.width, hit.textureCoord.y * drawTexture.height));
        }
    }

    /// <summary>
    /// Calls OCR to recognize string from the provided drawn texture and adds this string to the text field. Moreover,
    /// it clears the texture afterwards.
    /// </summary>
    private async Task RecognizeAsync()
    {
        loadingBar.transform.Find("loadingBar").gameObject.GetComponent<Text>().text = "Analyzing...";
        loadingBar.SetActive(true);

        string res = await ExecuteRecognitionAndRetrieveResults();
        if (string.IsNullOrEmpty(res) || string.IsNullOrWhiteSpace(res))
        {
            loadingBar.transform.Find("loadingBar").gameObject.GetComponent<Text>().text = "Try again...";
        }
        else
        {
            textField.AddString(((textField.GetCurrentWord(true) == "") ? "" : " ") + res.Substring(0, res.Length - 1));
            loadingBar.SetActive(false);
        }

        isDrawn = false;
        ClearTexture();
    }

    /// <summary>
    /// Function executes terminal process and retrieves recognized textual content from the drawn texture. The texture is
    /// firstly stored on the disc so that the OCR process can read the image. Also, the OCR process is timed to exactly
    /// 8 seconds max after which it returns empty string.
    /// </summary>
    /// <returns> Recognized textual data from image </returns>
    private async Task<string> ExecuteRecognitionAndRetrieveResults()
    {
        File.WriteAllBytes(Application.dataPath + "/StreamingAssets/Computer Vision/image.png", drawTexture.EncodeToPNG());
        ProcessStartInfo startInfo = new ProcessStartInfo()
        {
            WorkingDirectory = Application.dataPath + "/StreamingAssets/Computer Vision/",
            FileName = Application.dataPath + "/StreamingAssets/Computer Vision/MyHandwritingRecognition.exe",
            UseShellExecute = false, 
            ErrorDialog = true,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            CreateNoWindow = true,
            Arguments = ""
        };

        Process myProcess = new Process
        {
            StartInfo = startInfo
        };
        myProcess.Start();
        await myProcess.WaitForExitAsync();
        return myProcess.StandardOutput.ReadToEnd();
    }

    /// <summary>
    /// Clears dirty white board before exit.
    /// </summary>
    private void OnDisable()
    {
        ClearTexture(); 
    }
}