﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct ButtonStruct
{
    public bool trigger;
    public bool grip;
    public bool primary;
    public bool trackpad;
}

public enum Button
{
    leftTrigger,
    rightTrigger,
    leftGrip,
    rightGrip,
    leftPrimary,
    rightPrimary,
    leftTrackpad,
    rightTrackpad
}

/// <summary>
/// Provides numerous getter functions for input handling.
/// </summary>
public class VRControllers : MonoBehaviour
{
    private UnityEngine.XR.InputDevice lController;     // represents the left VR controller
    private UnityEngine.XR.InputDevice rController;     // represents the right VR controller
    
    // declares if bool getters return 'true' when the hand started pressing a button, not while pressing a button
    public ButtonStruct inputReturnsOnEntered;

    private Tuple<Button, bool> currentlyUsedButton = new Tuple<Button, bool>(Button.rightTrigger, false);  // occupies the input

    /// <summary>
    /// Resolves the actual return value for a 'bool' getter function.
    /// </summary>
    /// <param name="candidate"> A candidate retrieved from the input of a controller. </param>
    /// <param name="onEntered"> A bool switch of a button, that determines how the candidate is handled. </param>
    /// <returns> 
    /// Similar to the OnEntered vs. OnStay property of a collider. If 'onEntered' is true, then the getter returns 
    /// 'true' only when the hand started pressing a button. In the opposite case, the getter always returns a 
    /// 'candidate' value, meaning returns 'true' while the hand is pressing a button, and 'false' otherwise. Also, 
    /// if a button is being pressed, no other button can be pressed at the same time, whether the onEntered is true
    /// or not.
    /// </returns>
    private bool ReturnValueResolver(Tuple<Button, bool> candidate, bool onEntered)
    {
        // currently used button is released, let another candidate to occupy the input
        if (!currentlyUsedButton.Item2) 
        {
            currentlyUsedButton = candidate;
            return candidate.Item2;
        }
        // currently used button is being pressed and another candidate called -> ignore
        if (candidate.Item1 != currentlyUsedButton.Item1)   
        {
            return false;   
        }
        // input of currently used button changed, while onEntered is true -> update
        if (onEntered && candidate.Item2 != currentlyUsedButton.Item2) 
        {
            currentlyUsedButton = candidate;
            return candidate.Item2;
        }
        // input of currently used button is the same, while onEntered is true -> ignore
        if (onEntered)
        {
            return false;
        }
        // onEntered is false, let the input of currently used button be always updated
        currentlyUsedButton = candidate;
        return candidate.Item2;
    }

    void Start()
    {
        var devices = new List<UnityEngine.XR.InputDevice>();

        // Try to get a left controller
        UnityEngine.XR.InputDevices.GetDevicesAtXRNode(UnityEngine.XR.XRNode.LeftHand, devices);
        if (devices.Count > 0)
        {
             lController = devices[0];
        } else
        {
            Debug.LogError("Left controller is missing!");
        }

        // Try to get a right controller
        UnityEngine.XR.InputDevices.GetDevicesAtXRNode(UnityEngine.XR.XRNode.RightHand, devices);
        if (devices.Count > 0)
        {
            rController = devices[0];
        } else
        {
            Debug.LogError("Right controller is missing!");
        }
    }
    
    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> Returns the current 3D position of the right controller. </returns>
    public Vector3 GetRightPosition()
    {
        Vector3 vec = Vector3.zero;
        rController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.devicePosition, out vec);
        return vec;
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> Returns the current rotation of the right controller. </returns>
    public Quaternion GetRightRotation()
    {
        Quaternion quat;
        rController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.deviceRotation, out quat);
        return quat;
    }

    /// <summary>
    /// Sends a haptic impulse to the left controller.
    /// </summary>
    /// <param name="channel"> The channel to receive the impulse. </param>
    /// <param name="amplitude"> The normalized (0.0 to 1.0) amplitude value of the haptic impulse to play on the device. </param>
    /// <param name="duration"> The duration in seconds that the haptic impulse will play. </param>
    public void SendLeftHapticImpulse(uint channel, float amplitude, float duration)
    {
        lController.SendHapticImpulse(channel, amplitude, duration);
    }

    /// <summary>
    /// Sends a haptic impulse to the right controller.
    /// </summary>
    /// <param name="channel"> The channel to receive the impulse. </param>
    /// <param name="amplitude"> The normalized (0.0 to 1.0) amplitude value of the haptic impulse to play on the device. </param>
    /// <param name="duration"> The duration in seconds that the haptic impulse will play. </param>
    public void SendRightHapticImpulse(uint channel, float amplitude, float duration)
    {
        rController.SendHapticImpulse(channel, amplitude, duration);
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> Returns the current velocity of the left controller. </returns>
    public Vector3 GetLeftVelocity()
    {
        Vector3 vector = new Vector3(0.0f, 0.0f, 0.0f);
        lController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.deviceVelocity, out vector);
        return vector;
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> Returns the current velocity of the right controller. </returns>
    public Vector3 GetRightVelocity()
    {
        Vector3 vector = new Vector3(0.0f, 0.0f, 0.0f);
        rController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.deviceVelocity, out vector);
        return vector;
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> A bool indicating whether the trigger button of the left controller is currently pressed. </returns>
    public bool IsLeftTrigger()
    {
        lController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out bool isPressed);
        return ReturnValueResolver(new Tuple<Button, bool>(Button.leftTrigger, isPressed), inputReturnsOnEntered.trigger);
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> A bool indicating whether the trigger button of the right controller is currently pressed. </returns>
    public bool IsRightTrigger()
    {
        rController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out bool isPressed);
        return ReturnValueResolver(new Tuple<Button, bool>(Button.rightTrigger, isPressed), inputReturnsOnEntered.trigger);
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> A bool indicating whether the grip button of the left controller is currently pressed. </returns>
    public bool IsLeftGrip()
    {
        lController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.gripButton, out bool isPressed);
        return ReturnValueResolver(new Tuple<Button, bool>(Button.leftGrip, isPressed), inputReturnsOnEntered.grip);
    }
    
    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> A bool indicating whether the grip button of the right controller is currently pressed. </returns>
    public bool IsRightGrip()
    {
        rController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.gripButton, out bool isPressed);
        return ReturnValueResolver(new Tuple<Button, bool>(Button.rightGrip, isPressed), inputReturnsOnEntered.grip);
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> A bool indicating whether the primary button of the left controller is currently pressed. </returns>
    public bool IsLeftPrimary()
    {
        lController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.primaryButton, out bool isPressed);
        return ReturnValueResolver(new Tuple<Button, bool>(Button.leftPrimary, isPressed), inputReturnsOnEntered.primary);
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> A bool indicating whether the primary button of the right controller is currently pressed. </returns>
    public bool IsRightPrimary()
    {
        rController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.primaryButton, out bool isPressed);
        return ReturnValueResolver(new Tuple<Button, bool>(Button.rightPrimary, isPressed), inputReturnsOnEntered.primary);
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> A bool indicating whether the trackpad of the left controller is currently pressed. </returns>
    public bool IsLeftTrackpad()
    {
        lController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.primary2DAxisClick, out bool isPressed);
        return ReturnValueResolver(new Tuple<Button, bool>(Button.leftTrackpad, isPressed), inputReturnsOnEntered.trackpad);
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> A bool indicating whether the trackpad of the right controller is currently pressed. </returns>
    public bool IsRightTrackpad()
    {
        rController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.primary2DAxisClick, out bool isPressed);
        return ReturnValueResolver(new Tuple<Button, bool>(Button.rightTrackpad, isPressed), inputReturnsOnEntered.trackpad);
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> Returns the 2D position of the touch point from the trackpad of the left controller. </returns>
    public Vector2 GetLeftTrackpad()
    {
        Vector2 vector = new Vector2(0.0f, 0.0f);
        lController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.primary2DAxis, out vector);
        return vector;
    }

    /// <summary>
    /// VR controller getter function.
    /// </summary>
    /// <returns> Returns the 2D position of the touch point from the trackpad of the right controller. </returns>
    public Vector2 GetRightTrackpad()
    {
        Vector2 vector = new Vector2(0.0f, 0.0f);
        rController.TryGetFeatureValue(UnityEngine.XR.CommonUsages.primary2DAxis, out vector);
        return vector;
    }
}
