﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Globalization;
using System.Collections;

public class TestingMode : MonoBehaviour
{
    // SWITCHES
    public bool isAlphabeticMethod;         // switch between numeric and alphabetic testing
    public bool captureFirstButtonPress;    // switch between first button press or first character in the input field for start of the entry time

    // PHRASES
    public int phraseCount = 5;             // 16 to test all text phrases
    public int timeoutSec = 60;             // Seconds to wait before the next phrase is automatically shown (aka timeout; set <= 0 for disabling the timeout)
    private string phrasesFilePath;         // path to the phrases file
    private string numbersFilePath;         // path to the numbers file
    private string logfileFilePath;         // path to the testlog file
    private List<string> phrases;           // loaded phrases
    private List<string> numbers;              // loaded numbers
    System.Random rnd;                      // functionality of random used for shuffling and number generation
    public Text presentedText;              // represents the presented text for the user
    private int phraseIndex;                // index of currently presented phrase to the user - keeps track of what phrase goes next
    private int numberIndex;

    // CAPTURED DATA
    private float startTime,              // starting time of entry time 
                    phraseStart;            // time of showing this the phrase
    private int totalErrors,                // number of total errors during the testing process
                previousErrors;             // number of total errors from the previous check
    private int leftControllerPresses,    // Number of times user pressed left controller buttons (can be used to detect what controller is primarily used or if both are used for writing)
                rightControllerPresses;   // Number of times user pressed right controller buttons (can be used to detect what controller is primarily used or if both are used for writing)
    private float lastCharacterAddedTime;
    private List<float> characterAddingDelays; // Lists times since last character was added (i.e., if we write A in T = 0, B in T = 2 and C in T = 3, it will contain [0, 2, 1] -- may be good to see if users are "thinking" about character to add, e.g. during dictionary). 

    private VRControllers con;
    private TextField textField;
    private IEnumerator timeoutCoroutine = null;

    /// <summary>
    /// Initialize the testing mode, loads phrases, clears the input and shows a new phrase.
    /// </summary>
    private void OnEnable()
    {
        phraseIndex = 0;
        numberIndex = 0;

        logfileFilePath = Application.dataPath + "/StreamingAssets/Testing/testlog.txt";
        con = FindObjectOfType<VRControllers>();
        textField = FindObjectOfType<TextField>();
        rnd = new System.Random();

        LoadPhrases();
        LoadNumbers();
        ClearAndNext();
    }

    /// <summary>
    /// Clears the input before leaving...
    /// </summary>
    private void OnDisable()
    {
        textField.EraseText();

        if (timeoutCoroutine != null)
        {
            StopCoroutine(timeoutCoroutine);
        }
    }

    /// <summary>
    /// Prepares for the next testing phrase by resetting all variables to default state and shows the next phrase.
    /// </summary>
    private void ClearAndNext()
    {
        startTime = 0.0f;
        previousErrors = 0;
        totalErrors = 0;
        leftControllerPresses = 0;
        rightControllerPresses = 0;
        phraseStart = Time.time;
        characterAddingDelays = new List<float>();

        if (timeoutCoroutine != null)
        {
            StopCoroutine(timeoutCoroutine);
        }

        if (numberIndex >= phraseCount || phraseIndex >= phraseCount)
        {
            presentedText.text = "* TESTING FINISHED *";
        }
        else
        {
            if (isAlphabeticMethod)
            {
                SetNextRandomPhrase();
            }
            else
            {
                SetNewGeneratedNumber();
            }

            if (timeoutSec > 0)
            {
                timeoutCoroutine = TimeoutAndNext();
                StartCoroutine(timeoutCoroutine);
            }
        }

        textField.EraseText();
    }

    /// <summary>
    /// Loads phrases for alphabetic testing. In the case study, testAllPhrases is set to true, meaning 16 phrases are 
    /// loaded. In the user study only 5 phrases are taken from the set. These phrases are also shuffled each time the 
    /// testing mode is invoked.
    /// </summary>
    private void LoadPhrases()
    {
        phrasesFilePath = Application.dataPath + "/StreamingAssets/Testing/phrases user testing.txt";

        if (!File.Exists(phrasesFilePath))
        {
            return;
        }
        phrases = File.ReadAllLines(phrasesFilePath).Take(phraseCount).ToList();
        
        // Uncomment to shuffle the phrases randomly
        //phrases = phrases.OrderBy(x => rnd.Next()).ToList();
    }

    private void LoadNumbers()
    {
        numbersFilePath = Application.dataPath + "/StreamingAssets/Testing/numbers user testing.txt";

        if (!File.Exists(numbersFilePath))
        {
            return;
        }
        numbers = File.ReadAllLines(numbersFilePath).Take(phraseCount).ToList();//.Select(x => float.Parse(x, CultureInfo.InvariantCulture)).ToList();
    }

    /// <summary>
    /// "Generates" a new number by shuffling the order of "numbers". Then it is randomly chosen if the number will be
    /// decimal, positive, or negative.
    /// </summary>
    private void SetNewGeneratedNumber()
    {
        presentedText.text = numbers[numberIndex % phraseCount]; //  string.Format(CultureInfo.InvariantCulture, "{0}", numbers[numberIndex % phraseCount].ToString());
        ++numberIndex;
    }

    /// <summary>
    /// Displays the next phrase from the shuffled set. The phrases are in a cycle and their order is preserved after
    /// finishing the last phrase.
    /// </summary>
    private void SetNextRandomPhrase()
    {
        presentedText.text = phrases[phraseIndex % phraseCount];
        ++phraseIndex;
    }

    /// <summary>
    /// Checks for the start of the entry time. In case of captureFirstButtonPress, the trigger button has to be pressed.
    /// The button must be pressed after 1.5sec timeout of the newly displayed phrase. In case of "not" 
    /// captureFirstButtonPress, the entry time starts with the entry of the first character to the textField.
    /// </summary>
    private void Update()
    {
        if (captureFirstButtonPress && startTime == 0.0f && (con.IsLeftTrigger() || con.IsRightTrigger()))
        {
            startTime = Time.time;
        }

        if (!captureFirstButtonPress && startTime == 0.0f && textField.GetText() != "")
        {
            startTime = Time.time;
        }

        if(con.IsLeftTrigger() || con.IsLeftPrimary() || con.IsLeftTrackpad())
        {
            ++leftControllerPresses;
        }

        if(con.IsRightTrigger() || con.IsRightPrimary() || con.IsRightTrackpad())
        {
            ++rightControllerPresses;
        }
    }

    /// <summary>
    /// Updates total mistakes, produced during the whole input process of currently shown presented text. The algorithm
    /// works as follows. Let's say user starts typing into the input field - in this case "all" parameter is false and 
    /// the transcribed text is only compared with a prefix of the presented text of the same length. Computes its string 
    /// distance and if the number of mistakes is higher than in the previous situation, updates the total errors by
    /// adding the difference between current and previous number of errors. After the user hits the OK button, "all" is 
    /// set to true and transcribed text is compared with the whole presented text.
    /// </summary>
    /// <param name="all"> Determines whether to compare transcribed text with the whole presented text. </param>
    public void CheckMistakes(bool all)
    {
        // TODO This is an ugly hack. This function is called by TextField.AddCharacter() with all=false
        //      so I just injected this code here.
        if(!all)
        {
            if(characterAddingDelays.Count == 0)
            {
                lastCharacterAddedTime = Time.time;
                characterAddingDelays.Add(0);
            }
            else
            {
                characterAddingDelays.Add(Time.time - lastCharacterAddedTime);
                lastCharacterAddedTime = Time.time;
            }
        }
        //

        int _new;
        if (!all)
        {
            int prefixLength = (presentedText.text.Length < textField.GetText().Length) ? presentedText.text.Length : textField.GetText().Length;
            _new = NGramSuggester.LevenshteinDistance(presentedText.text.Substring(0, prefixLength), textField.GetText());
        } else
        {
            _new = NGramSuggester.LevenshteinDistance(presentedText.text, textField.GetText());
        }
        
        if (previousErrors < _new)
        {
            totalErrors += _new - previousErrors;
        }
        previousErrors = _new;
    }

    /// <summary>
    /// Computes the entry time, wpm/cpm and all error rates based on the updated variables, such as startTime, totalErrors.
    /// Then, this entry is saved to the testlog file. Moreover, if the string distance between presented and transcribed
    /// text is more than 5, abort.
    /// </summary>
    public void EvaluateAndNext()
    {
        int LD = NGramSuggester.LevenshteinDistance(presentedText.text, textField.GetText());
        if (LD > 5)
        {
            return;
        }

        EnforceEvaluateAndNext(LD);
    }

    public void SkipAndNext()
    {
        EnforceEvaluateAndNext(NGramSuggester.LevenshteinDistance(presentedText.text, textField.GetText()), true);
    }

    public IEnumerator TimeoutAndNext()
    {
        yield return new WaitForSeconds(timeoutSec);

        EnforceEvaluateAndNext(NGramSuggester.LevenshteinDistance(presentedText.text, textField.GetText()), true);
    }

    private void EnforceEvaluateAndNext(int LD, bool timeout = false)
    {
        float finalTime = Time.time;
        CheckMistakes(true);

        int C = Mathf.Max(textField.GetText().Length, presentedText.text.Length) - LD,
            INF = LD,
            IF = totalErrors - INF;

        float wpm = textField.GetText().Length / (finalTime - startTime) * 60.0f * ((isAlphabeticMethod) ? 0.2f : 1.0f),
            uncorrectedER = INF * 100.0f / (C + INF + IF),
            correctedER = IF * 100.0f / (C + INF + IF),
            totalER = uncorrectedER + correctedER;

        if (File.Exists(logfileFilePath))
        {
            File.AppendAllText(logfileFilePath,
                DateTime.Now.ToString() + '\t' +
                phraseStart.ToString(CultureInfo.InvariantCulture) + '\t' +
                startTime.ToString(CultureInfo.InvariantCulture) + '\t' +
                finalTime.ToString(CultureInfo.InvariantCulture) + '\t' +
                ((isAlphabeticMethod) ? "Alphabetic" : "Numeric") + '\t' +
                SceneManager.GetActiveScene().name + '\t' +
                presentedText.text + '\t' +
                textField.GetText() + '\t' +
                wpm.ToString(CultureInfo.InvariantCulture) + '\t' +
                correctedER.ToString(CultureInfo.InvariantCulture) + '\t' +
                uncorrectedER.ToString(CultureInfo.InvariantCulture) + '\t' +
                totalER.ToString(CultureInfo.InvariantCulture) + '\t' +
                leftControllerPresses.ToString() + '\t' +
                rightControllerPresses.ToString() + '\t' +
                String.Join(";", characterAddingDelays.Select(x => x.ToString(CultureInfo.InvariantCulture))) +
            (timeout ? "\t skip" : "\t ok") + 
                Environment.NewLine);
        }
        else
        {
            Debug.Log("Error accessing log file!");
        }
        ClearAndNext();
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Skip sentence/number"))
        {
            SkipAndNext();
        }
    }
}
