﻿using UnityEngine;
using UnityEngine.UI;

public class TrackPadCylinderController : MonoBehaviour
{
    public Text[] textComponents = new Text[4];         // characters of this trackpad cylinder
    private VRControllers controllers;
    private TextField textField;

    void Start()
    {
        controllers = FindObjectOfType<VRControllers>();
        textField = FindObjectOfType<TextField>();
    }

    /// <summary>
    /// Resets all highlights to default position.
    /// </summary>
    public void ResetHighlights()
    {
        foreach (Text t in textComponents)    // resets the state of highlights
        {
            t.fontStyle = FontStyle.Normal;
            if (gameObject.name == "Cylinder 10" || gameObject.name == "Cylinder 11")
            {
                t.color = Color.white;
            } else
            {
                t.color = Color.black;
            }
        }
    }

    /// <summary>
    /// Applies a highlight to the letter that is being selected by the touch on the trackpad of controller.
    /// </summary>
    /// <param name="isRight"> Right or left controller switch. </param>
    public void Highlights(bool isRight)
    {
        ResetHighlights();
        Vector2 axis = (isRight) ? controllers.GetRightTrackpad() : controllers.GetLeftTrackpad();

        int i = -1;
        if (0.5f < axis[0])
        {
            i = 2;
        } else if (-0.5f >= axis[0])
        {
            i = 1;
        } else if (0.5f < axis[1])
        {
            i = 0;
        } else if (-0.5f >= axis[1])
        {
            i = 3;
        }
        if (i != -1)
        {
            textComponents[i].fontStyle = FontStyle.Bold;
            textComponents[i].color = Color.red;
        }
    }

    /// <summary>
    /// Handles input for a single Trackpad Cylinder. Depending on the click position of the trackpad, there are several
    /// input options. The function not only handles the input from a-z, but also to move the cursor, insert space,
    /// backspace and shift functionality.
    /// </summary>
    public void TrackpadClicked()
    {        
        Vector2 axis2D = (controllers.IsRightTrackpad()) ? controllers.GetRightTrackpad() : controllers.GetLeftTrackpad();
        bool shift = false;

        if ( 0.5f < axis2D[0] )
        {
            if (textComponents[2].text == "►")
            {
                textField.MoveCursorRight();
            } else
            {
                textField.AddCharacter(textComponents[2].text);
            }
        }
        else if (-0.5f >= axis2D[0])
        {
            if (textComponents[1].text == "◄")
            {
                textField.MoveCursorLeft();
            } else
            {
                textField.AddCharacter(textComponents[1].text);
            }
        }
        else if (0.5f < axis2D[1])
        {
            if (textComponents[0].text == "Space")
            {
                textField.AddCharacter(" ");
            } else if (textComponents[0].text == "Shift")
            {
                ShiftTriggered(true);
                textComponents[0].text = "Shift ";
                shift = true;
            } else if (textComponents[0].text == "Shift ")
            {
                ShiftTriggered(false);
                textComponents[0].text = "Shift";
            } 
            else
            {
                textField.AddCharacter(textComponents[0].text);
            }
        }
        else if (-0.5f >= axis2D[1])
        {
            if (textComponents[3].text == "Backspace")
            {
                textField.EraseCharacter();
            } else
            {
                textField.AddCharacter(textComponents[3].text);
            }
        }
        
        if (!shift)
        {
            ShiftTriggered(false);
        }
    }

    /// <summary>
    /// Triggers a "shift" key for letters of all cylinders.
    /// </summary>
    /// <param name="up"> ToUpper vs ToLower switch. </param>
    private void ShiftTriggered(bool up)
    {
        GameObject[] cylinders = new GameObject[8];
        for (int i = 1; i < 9; ++i)
        {
            cylinders[i - 1] = GameObject.Find("Cylinder " + i);
        }

        foreach (GameObject cylinder in cylinders)
        {
            foreach (Transform child in cylinder.transform)
            {
                foreach (Transform textObject in child.gameObject.transform)
                {
                    textObject.gameObject.GetComponent<Text>().text = (up) ? textObject.gameObject.GetComponent<Text>().text.ToUpper() 
                                                                           : textObject.gameObject.GetComponent<Text>().text.ToLower();
                }
            }
        }
    } 
}
