﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class TrackpadAlphaHighlightsController : MonoBehaviour
{
    public XRRayInteractor interactorR, interactorL;    // ray interactors of both controllers;
    private VRControllers controllers;

    private void Start()
    {
        controllers = FindObjectOfType<VRControllers>();
    }

    /// <summary>
    /// Controls the highlights of letters on a currently selected trackpad. Moreover, checks for trigger button press
    /// on the Menu UI.
    /// </summary>
    private void Update()
    {
        interactorR.GetCurrentRaycastHit(out var hitR);
        interactorL.GetCurrentRaycastHit(out var hitL);

        if (hitL.collider != null && hitL.collider.gameObject.name != "Button")
        {
            TrackPadCylinderController c = hitL.collider.gameObject.GetComponent<TrackPadCylinderController>();
            if (c != null)
            {
                c.Highlights(false);
            }
        }

        if (controllers.IsLeftTrigger() && hitL.collider != null && hitL.collider.gameObject.name == "Button")
        {
            XRSimpleInteractable interactable = hitL.collider.gameObject.GetComponent<XRSimpleInteractable>();
            interactable.onSelectEntered.Invoke(interactorL);
            controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
        }

        if (hitR.collider != null && hitR.collider.gameObject.name != "Button")
        {
            TrackPadCylinderController c = hitR.collider.gameObject.GetComponent<TrackPadCylinderController>();
            if (c != null)
            {
                c.Highlights(true);
            }
        }

        if ((controllers.IsRightTrigger() && hitR.collider != null && hitR.collider.gameObject.name == "Button"))
        {
            XRSimpleInteractable interactable = hitR.collider.gameObject.GetComponent<XRSimpleInteractable>();
            interactable.onSelectEntered.Invoke(interactorR);
            controllers.SendRightHapticImpulse(0, 1.0f, 0.0f);
        }

        controllers.IsRightTrackpad();
        controllers.IsLeftTrackpad();
    }
}
