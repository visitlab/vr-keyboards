﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class physicalButtonsRaycastController : MonoBehaviour
{
    public GameObject left, right, menu;
    private bool raycastActive;

    private void Start()
    {
        raycastActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (menu.activeSelf && !raycastActive)
        {
            SwitchComponents(right, true);
            SwitchComponents(left, true);
        }

        if (!menu.activeSelf && raycastActive)
        {
            SwitchComponents(right, false);
            SwitchComponents(left, false);
        }
    }

    private void SwitchComponents(GameObject o, bool state)
    {
        o.GetComponent<LineRenderer>().enabled = state;
        o.GetComponent<XRInteractorLineVisual>().enabled = state;
        raycastActive = state;
    }
}
