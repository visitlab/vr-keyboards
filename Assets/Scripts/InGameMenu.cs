﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenu : MonoBehaviour
{
    private VRControllers controllers;
    private GameObject menuInterface, testInterface;

    // Start is called before the first frame update
    void Start()
    {
        controllers = FindObjectOfType<VRControllers>();
        menuInterface = transform.Find("Menu GUI").gameObject;
        testInterface = transform.Find("Test GUI").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (controllers.IsRightGrip())
        {
            menuInterface.SetActive(!menuInterface.activeSelf);
        }
    }

    public void SwitchTestGUI()
    {
        testInterface.SetActive(!testInterface.activeSelf);
        menuInterface.SetActive(false);
    }

    /// <summary>
    /// Used to open a specific keyboard scene from the main menu.
    /// </summary>
    /// <param name="name"> Name of the keyboard scene. </param>
    public void OpenScene(string name)
    {
        SceneManager.LoadSceneAsync(name);
    }
}
