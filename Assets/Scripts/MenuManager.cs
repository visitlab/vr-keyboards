﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    private void Start()
    {
        FindObjectOfType<ChangeSkybox>().Edit(FindObjectOfType<ChangeSkybox>().AppliedSkybox());
    }


    /// <summary>
    /// Called on the exit button and quits the application.
    /// </summary>
    public void Exit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Used to open a specific keyboard scene from the main menu.
    /// </summary>
    /// <param name="name"> Name of the keyboard scene. </param>
    public void OpenScene(string name)
    {
        SceneManager.LoadSceneAsync(name);
    }

    /// <summary>
    /// Changes skybox depending on the index.
    /// </summary>
    /// <param name="i"> The index. </param>
    public void ChangeBackground(int i)
    {
        FindObjectOfType<ChangeSkybox>().Edit(i);
    }
}
